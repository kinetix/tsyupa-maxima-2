package homework08;

public class Program1 {
    static int sumOfDigits(int number) {
        // на входе
        System.out.println("--> f(" + number + ")");

        int lastDigit = number % 10;
        number = number / 10;
        if (number > 0) {
            lastDigit += sumOfDigits(number);
        }
        // на выходе
        System.out.println("<-- f() = " + lastDigit);

        return lastDigit;
    }

    public static void main(String[] args) {
        System.out.println(sumOfDigits(45678));
    }
}