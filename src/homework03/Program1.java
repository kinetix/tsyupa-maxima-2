package homework03;

import java.util.Scanner;

public class Program1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int digitsSum = 0;
        int min = 0;
        int newNumber = 0;


        while (number != -1) {
            int numberOfMin = number;
            while (number != 0) {
                int lastDigit = number % 10;
                number = number / 10;
                digitsSum += lastDigit;
            }
            if (min == 0) {
                min = digitsSum;
            } else if (min > digitsSum) {
                min = digitsSum;
                newNumber = numberOfMin;
            }
            digitsSum = 0;
            number = scanner.nextInt();
        }

        System.out.println("Число - " + newNumber + ", Сумма цифр - " + min);
    }
}
