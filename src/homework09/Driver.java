package homework09;

public class Driver {
    private String name;
    private int experience;
    private Bus bus;

    public Driver(String name, int experience) {
        this.name = name;
        if (experience > 0) {
            this.experience = experience;
        } else {
            System.err.println("ОПЫТА у " + name +  "НЕТ!");
        }
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public void drive(Bus bus) {
        bus.setIsDrived(true);
        System.out.println("Автобус поехал. Его пассажиры: ");
        for(int i = 0; i < bus.getPassengers().length; i++) {
            System.out.println(bus.getPassengers()[i].getName());
        }
    }

    public String getName() {
        return name;
    }
    public void stop(Bus bus) {
        bus.setIsDrived(false);
        System.out.println("Автобус остановился");
    }
}
