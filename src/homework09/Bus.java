package homework09;

public class Bus {
    private int number;
    private String model;
    private Driver driver;
    private boolean isDrived;

    // массив пассажиров
    private Passenger[] passengers;
    // фактическое количество пассажиров на данный момент
    private int count;

    public Bus(int number, String model, int placesCount) {
        if (number > 0) {
            this.number = number;
        } else {
            this.number = 1;
        }
        this.model = model;
        // создали placesCount объектных переменных в которые можно положить пассажира
        this.passengers = new Passenger[placesCount];
        this.isDrived = false;
    }

    public void incomePassenger(Passenger passenger) {
        // проверяем, не превысили ли мы количество мест?
        if (this.count < this.passengers.length) {
            this.passengers[count] = passenger;
            this.count++;
            System.out.println(passenger.getName() + " зашел в автобус");
        } else {
            System.err.println("Автобус переполнен!");
        }
    }

    public void setDriver(Driver driver) {
        if (this.isDrived) {
            System.err.println("Автобус едет, водителя нельзя поменять");
        }
        else {
            this.driver = driver;
            driver.setBus(this);
            System.out.println("Водитель автобуса " + this.getDriver().getName());
        }
    }

    public boolean isFull() {
        return this.count == passengers.length;
    }

    public Driver getDriver() {
        return driver;
    }
    public void setIsDrived(boolean drived) {
        isDrived = drived;
    }

    public Passenger[] getPassengers() {
        return passengers;
    }
}
