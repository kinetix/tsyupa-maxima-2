package homework05;

import java.util.Scanner;

public class Program1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[121];
        int age = scanner.nextInt();

        while (age != -1) {
            array[age]++;
            age = scanner.nextInt();
        }

        int count = 0;
        int max = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > count) {
                count = array[i];
                max = i;
            }
        }

        System.out.println(max);
    }
}