package homework06;

import java.util.Arrays;
import java.util.Scanner;

public class Program1 {
    public static void main(String[] args) {
        int[] array = {24, 12, 54, 76, 23, 71, 54, 76, -6, 11, 65, 87};
        Scanner scanner = new Scanner(System.in);

        System.out.println("Исходный: " + Arrays.toString(array));
        System.out.print("Отсортированный: ");
        selectionSort(array);
        System.out.println("Введите число для поиска:");
        int element = scanner.nextInt();
        System.out.println(binarySearch(array, element));
    }

    static void selectionSort(int array[]) {
        int min, indexOfMin;

        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }
        System.out.println(Arrays.toString(array));
    }

    static boolean binarySearch(int array[], int element) {
        boolean hasElement = false;
        int left = 0;
        int right = array.length - 1;
        int middle = left + (right - left) / 2;

        while (left <= right) {
            if (element < array[middle]) {
                right = middle - 1;
                middle = left + (right - left) / 2;
            } else if (element > array[middle]) {
                left = middle + 1;
                middle = left + (right - left) / 2;
            } else {
                hasElement = true;
                break;
            }
        }
        return hasElement;
    }
}
